package de.hpi.javaide.breakout.elements;

import java.awt.Dimension;
import java.awt.Point;

import de.hpi.javaide.breakout.basics.Rectangular;
import de.hpi.javaide.breakout.starter.Game;
import de.hpi.javaide.breakout.starter.GameConstants;
import processing.core.PApplet;

//TODO wichtige Attribute: Größe, Position, Abstand der Bricks untereinander
//     Irgendwie muss ich herausbekommen ob der Stein noch existiert oder nicht.
public class Brick extends Rectangular {
	private int live;
	public Brick(Game game, Point position, Dimension dimension) {
		super(game, position, dimension);
		// TODO Auto-generated constructor stub
		setColor(150, 100, 50);
		live= GameConstants.LIVES;
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		game.rectMode(PApplet.CORNER);
		game.noStroke();
		game.fill(getR(), getG(), getB());
		game.rect(getX(), getY(), getWidth(), getHeight());		
	}
	public boolean takeLive() {
		setColor(getR()*2/3, getG()*2/3, getB()*2/3);
		return --live <= 0;
	}
}
