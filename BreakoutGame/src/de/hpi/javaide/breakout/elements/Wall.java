package de.hpi.javaide.breakout.elements;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;

import de.hpi.javaide.breakout.Displayable;
import de.hpi.javaide.breakout.basics.CollisionObject;
import de.hpi.javaide.breakout.starter.Game;
import de.hpi.javaide.breakout.starter.GameConstants;

/**
 * Blueprint for the Wall
 * 
 * @author Ralf Teusner and Tom Staubitz
 *
 */
//TODO die Wall wird aus Bricks gebaut.
public class Wall implements Displayable, Iterable<Brick> {
	
	/**
	 * Datastructure to keep the Bricks
	 */
	private ArrayList<Brick> wall;


	public Wall(Game game, int i, int j) {
		// TODO Auto-generated constructor stub
		wall= new ArrayList<Brick>();
		buildWall(game, i, j);
	}
	@Override
	public Iterator<Brick> iterator() {
		return wall.iterator();
	}
	/**
	 * Build the wall by putting the single bricks into their position
	 * Hint: You might want to use one or two for-loops
	 * 
	 * @param game
	 * @param columns
	 * @param rows
	 */
	private void buildWall(Game game, int columns, int rows) {
		int laenge= (GameConstants.SCREEN_X - 50) / 9;
		int hoehe= (GameConstants.SCREEN_Y - 50) / 9;
		for (int y= 0; y < 2; y++) {
			for (int x= 0; x < 9; x++) {
				wall.add(new Brick(game, new Point(x*laenge +  5*(x+1), y*hoehe + 5*(y+1)), new Dimension(laenge, hoehe)));
			}
		}
	}
	@Override
	public void display() {
		// TODO Auto-generated method stub
		for (Brick brick: wall) {
			brick.display();
		}
	}
	public void hitBrick(int steinNr){
		// TODO
		if (wall.get(steinNr).takeLive()) {
			wall.remove(steinNr);
		}
	}
	public int size() {
		return wall.size();
	}
	public CollisionObject getBrick(int i) {
		// TODO Auto-generated method stub
		return wall.get(i);
	}
}
