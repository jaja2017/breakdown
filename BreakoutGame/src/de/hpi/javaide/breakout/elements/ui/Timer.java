package de.hpi.javaide.breakout.elements.ui;

import de.hpi.javaide.breakout.basics.Font;
import de.hpi.javaide.breakout.basics.UIObject;
import de.hpi.javaide.breakout.starter.Game;
import de.hpi.javaide.breakout.starter.GameConstants;
import processing.core.PApplet;

public class Timer extends UIObject {

	private int seconds;
	private int lastSecond;
	

	public Timer(Game game) {
		super(game);
		seconds = 60;
		lastSecond= PApplet.second();
	}

	@Override
	public void display() {
		game.fill(255);
		game.textFont(Font.getFont16());
		game.text("Time left: " + seconds, game.width-GameConstants.LIVES*(GameConstants.DURCHMESSER + 5), game.height-2*(GameConstants.DURCHMESSER+5));
	}

	@Override
	public void update(String input) {
		// TODO Auto-generated method stub
		int now= PApplet.second();
		if (now != lastSecond) {
			seconds--;
			lastSecond= now;
		}
	}
	public int getSeconds() {
		return seconds;
	}
}
