package de.hpi.javaide.breakout.elements.ui;

import de.hpi.javaide.breakout.basics.Font;
import de.hpi.javaide.breakout.basics.UIObject;
import de.hpi.javaide.breakout.starter.Game;

//TODO nach dem die fehlenden Methoden ergänzt wurden, muss hier noch ein Konstruktorparameter 
//     an das zugehörige Attribut übergeben werden.

public class Info extends UIObject {

	private String content;
	
	public Info(Game game, String content) {
		super(game);
		this.content= content;
	}

	@Override
	public void display() {
	    game.textFont(Font.getFont24());
	    // TODO https://open.hpi.de/courses/javawork2017/question/ad2d7f25-a4da-4fa1-af51-642440502e53
	    game.text(content,50,50); 
	}

	@Override
	public void update(String input) {
		// TODO Auto-generated method stub
		content= input;
	}
}
