package de.hpi.javaide.breakout.elements.ui;

import de.hpi.javaide.breakout.basics.Font;
import de.hpi.javaide.breakout.basics.UIObject;
import de.hpi.javaide.breakout.starter.Game;
import de.hpi.javaide.breakout.starter.GameConstants;

public class Score extends UIObject {

	private int score;
	
	public Score(Game game) {
		super(game);
		score= 0;
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		game.fill(255);
		game.textFont(Font.getFont16());
		game.text("Score: " + score, game.width-GameConstants.LIVES*(GameConstants.DURCHMESSER + 5), game.height-3*(GameConstants.DURCHMESSER+5));
	}

	@Override
	public void update(String input) {
		// TODO Auto-generated method stub
		score++;
	}
	public int getScore() {
		return score;
	}
}
