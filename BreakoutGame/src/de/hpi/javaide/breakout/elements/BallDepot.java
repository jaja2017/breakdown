package de.hpi.javaide.breakout.elements;

import java.awt.Point;
import java.util.ArrayList;

import de.hpi.javaide.breakout.Displayable;
import de.hpi.javaide.breakout.Measureable;
import de.hpi.javaide.breakout.basics.Vector;
import de.hpi.javaide.breakout.starter.Game;
import de.hpi.javaide.breakout.starter.GameConstants;

//TODO hier werden wir sicher eine Collection brauchen um die Bälle unterzubringen.
//     Vermutlich werden wir wissen wollen wann das Depot leer ist.
//     Irgendwie müssen die Bälle an den Start gebracht werden.
public class BallDepot implements Displayable, Measureable {
	private ArrayList<Ball> depot= new ArrayList<Ball>();
	public BallDepot(Game game) {
		// TODO Auto-generated constructor stub
		for (int i= 0; i < GameConstants.LIVES; i++) {
			depot.add(new Ball(game, new Point(game.width - (i + 1)*(GameConstants.DURCHMESSER + 5),
					game.height - (GameConstants.DURCHMESSER + 5))));
		}
	}

	@Override
	public int getX() {
		// TODO Auto-generated method stub
		// man koennte den Mittelwert aller Baelle im depo berechnen
		// oder den Wert des zu Beginn mitleren Balls
		return (int) (GameConstants.SCREEN_X - (GameConstants.LIVES / 2.0)*(GameConstants.DURCHMESSER + 5));
	}

	@Override
	public int getY() {
		// TODO Auto-generated method stub
		return GameConstants.SCREEN_Y - (GameConstants.DURCHMESSER + 5);
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return GameConstants.SCREEN_X ;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return GameConstants.SCREEN_Y;
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		for (Ball ball : depot) {
			ball.display();
		}
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return depot.size() == 0;
	}

	public Ball dispense() {
		// TODO Auto-generated method stub
		if (depot.size() > 0) {
			Ball ball= depot.remove(depot.size() - 1);
			ball.setDirection(new Vector(-5,-5));
			return ball;
		} else {
			return null;
		}
	}

}
