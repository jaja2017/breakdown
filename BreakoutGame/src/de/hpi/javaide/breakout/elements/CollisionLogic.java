package de.hpi.javaide.breakout.elements;

import java.awt.Dimension;
import java.awt.Point;
import de.hpi.javaide.breakout.basics.Vector;
import de.hpi.javaide.breakout.starter.Game;

//TODO den Fehler unten haben wir absichtlich eingebaut, um zu zeigen, dass hier noch was getan werden muss.
//     Hier sollen alle Kollisionen geprüft werden. Trifft der Ball das Paddle.
//     Für jeden Stein in der Mauer: wurde er getroffen?
//     Erreicht der Ball den Spielfeld Rand.
//     Tipp: Schleifen könnten sich als hilfreich erweisen.
public class CollisionLogic {
	/**
	 * The constructor of this class is private to make sure that it is only used as a static class.
	 * - it cannot be instantiated,
	 * - it cannot hold a state,
	 * - it contains only static methods
	 */
	private CollisionLogic() {}
	
	/**
	 * This method provides a way to determine if the ball collides with any of the collidable elements on the screen.
	 * Paddle, Bricks, ...
	 * 
	 * @param game
	 * @param ball
	 * @param paddle
	 * @param wall
	 */
	public static void checkCollision(Game game, Ball ball, Paddle paddle, Wall wall) {
		// TODO todo 
		// Kollision mit den Waenden - Reflektion
		if (ball.getX() - ball.getWidth() / 2 <= 0 || ball.getX() + ball.getWidth() / 2 >= game.width) {
			ball.getDirection().setX(-ball.getDirection().getX());
		}
		// Kollision mit dem Paddle - Reflektion
		if (ball.getY() >= paddle.getY() - paddle.getHeight() && ball.getX() >= paddle.getX() - paddle.getWidth() / 2 && ball.getX() <= paddle.getX() + paddle.getWidth() / 2) {
			ball.getDirection().setY(-Math.abs(ball.getDirection().getY() + paddle.getSpeed()/3));
			ball.getDirection().setX(ball.getDirection().getX() + paddle.getSpeed()*2/3);
		}
		// Kollision mit der Wall
		for (int i= 0; i < wall.size(); i++) {
			// ball.getGeometry().intersects((Rectangle2D) wall.getBrick(i).getGeometry())
			if ((ball.getY() - ball.getWidth() / 2 <= wall.getBrick(i).getY() + wall.getBrick(i).getHeight() 
					&& ball.getY() + ball.getWidth() / 2 >= wall.getBrick(i).getY())
					&& (ball.getX() - ball.getWidth() / 2 <= wall.getBrick(i).getX() + wall.getBrick(i).getWidth() 
					&& ball.getX() + ball.getWidth() / 2 >= wall.getBrick(i).getX())) {
				game.increaseScore(1);
				// Kollision mit der Horizontalen Seite
				if (ball.getY() - ball.getWidth() / 4  >= wall.getBrick(i).getY() + wall.getBrick(i).getHeight() 
						|| ball.getY() + ball.getWidth() / 2 <= wall.getBrick(i).getY()) {
					if (ball.getY() - ball.getWidth() / 2  >= wall.getBrick(i).getY() + wall.getBrick(i).getHeight()) {
						ball.getDirection().setY(Math.abs(ball.getDirection().getY()
								+Math.abs(wall.getBrick(i).getY() + wall.getBrick(i).getHeight() - (ball.getY() - ball.getWidth() / 2))));
						// "Ball von unten"
					} else
						ball.getDirection().setY(-(ball.getDirection().getY()));
					    // "Ball von oben"
				} else {
					ball.getDirection().setX(-ball.getDirection().getX());
					// "Ball von der Seite"
				}
				wall.hitBrick(i);
			}
		}
		// Kollision mit der Decke - Reflektion
		if (ball.getY() - ball.getWidth() / 2 <= 0) {
			ball.getDirection().setY(-ball.getDirection().getY());
		}
		// Kollision mit dem Boden - Ball platt
		if (ball.getY() + ball.getWidth() / 2 >= game.height) {
			ball.setDirection(new Vector(0,0));
			for (int i= ball.getWidth(); i > 0; i--) {
				ball.update(new Point(ball.getX(), ball.getY()), new Dimension(ball.getWidth(), i));
			}
			ball= null;
		}
	}

}
