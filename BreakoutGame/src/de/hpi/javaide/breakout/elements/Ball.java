package de.hpi.javaide.breakout.elements;

import java.awt.Dimension;
import java.awt.Point;

import de.hpi.javaide.breakout.basics.Elliptic;
import de.hpi.javaide.breakout.basics.Vector;
import de.hpi.javaide.breakout.starter.Game;
import de.hpi.javaide.breakout.starter.GameConstants;
import processing.core.PApplet;

/**
 * Blueprint for a Ball.
 *
 * @author Ralf Teusner and Tom Staubitz
 *
 */
//TODO neben dem Ergänzen der vom Interface erwarteten Methoden,
//     sollte der Ball Eigenschaften wie Größe und Richtung mitbringen.
//     Richtung wird in der Regel als Vector definiert.
//     Vermutlich sollte er die Richtung ändern können und sehr
//     wahrscheinlich wird früher oder später
//     jemand wissen wollen in welche Richtung er fliegt.

public final class Ball extends Elliptic {
	private Vector direction;
	/**
	 * @param game
	 * @param position
	 */
	public Ball(final Game game, final Point position) {
		super(game, position, new Dimension(GameConstants.DURCHMESSER, GameConstants.DURCHMESSER));
		direction= new Vector(0,0);
		setColor(124, 124, 0);
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		// damit man mal einen Ball sehen kann ..
		game.ellipseMode(PApplet.CENTER);
		game.noStroke();
		game.fill(getR(), getG(), getB());
		game.ellipse(getX(), getY(), getWidth(), getHeight());
	}

	/**
	 * bewegt den Ball
	 */
	public void move() {
		// TODO Auto-generated method stub
		update(new Point(getX() + (int) getDirection().getX(), getY()
				+ (int) getDirection().getY()),
				new Dimension(getWidth(), getHeight()));
	}


	/**
	 * @return the direction
	 */
	public Vector getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(final Vector direction) {
		this.direction = direction;
	}

	/**
	 * @return ob sich der Ball noch bewegt
	 */
	public boolean isMoving() {
		// TODO Auto-generated method stub
		return getDirection().getX() != 0 || getDirection().getY() != 0;
	}
}