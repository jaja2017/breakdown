package de.hpi.javaide.breakout.starter;

import java.awt.Point;

public interface GameConstants {
	int LIVES = 3;
	int SCREEN_X = 1024; // mein Bildschirm 1280 x 1024
	int SCREEN_Y = 768;  // XGA 1024x768 sollte jeder haben
	Point STARTPOSITION = new Point(SCREEN_X/2, SCREEN_Y/2);
	int DURCHMESSER= (GameConstants.SCREEN_X + GameConstants.SCREEN_Y) / 40;

}
